#![recursion_limit = "1024"]

use wasm_bindgen::prelude::*;
use yew::start_app;

pub mod app;

#[wasm_bindgen]
pub fn run_app() {
    start_app::<app::App>();
}
