use yew::*;

pub struct App {
    link: ComponentLink<Self>,
    props: Props,
}

#[derive(Clone, Properties, Debug, Default)]
pub struct Props {}

#[derive(Clone, Debug)]
pub enum Msg {
    Ignore,
}

impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { link, props }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => return false,
        }
        true
    }

    fn view(&self) -> Html {
        html! {
            <div>
                {"Hello World"}
            </div>
        }
    }
}
