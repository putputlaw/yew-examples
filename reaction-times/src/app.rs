use crate::random::random;
use chrono::*;
use std::collections::VecDeque;
use yew::services::{timeout::TimeoutTask, TimeoutService};
use yew::*;

pub struct App {
    link: ComponentLink<Self>,
    props: Props,
    state: State,
    time_begin: Option<DateTime<Utc>>,
    time_end: Option<DateTime<Utc>>,
    timeout: Option<TimeoutTask>,
    history: VecDeque<f64>,
}

#[derive(Debug, Clone, Copy)]
pub enum State {
    Init,
    DenyClick,
    AcceptClick,
    Show,
}

#[derive(Clone, Properties, Debug, Default)]
pub struct Props {}

#[derive(Clone, Debug)]
pub enum Msg {
    Ignore,
    Click,
    AllowClick,
}

impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            props,
            state: State::Init,
            timeout: None,
            time_begin: None,
            time_end: None,
            history: VecDeque::default(),
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => return false,
            Msg::Click => match self.state {
                State::Init | State::Show => {
                    self.state = State::DenyClick;
                    let millis = 1_000 + (4_000_f64 * random()).round() as u64;
                    self.timeout = Some(TimeoutService::new().spawn(
                        std::time::Duration::from_millis(millis),
                        self.link.callback(|_| Msg::AllowClick),
                    ));
                }
                State::DenyClick => {
                    self.state = State::Init;
                }
                State::AcceptClick => {
                    self.state = State::Show;
                    self.time_end = Some(Utc::now());
                    if let Some(duration) = self.duration_millis() {
                        self.history.push_front(duration);
                    }
                }
            },
            Msg::AllowClick => {
                self.timeout = None;
                if let State::DenyClick = self.state {
                    self.state = State::AcceptClick;
                }
                self.time_begin = Some(Utc::now());
                self.time_end = None;
            }
        }
        true
    }

    fn view(&self) -> Html {
        let style = format!(
            "
            background-color: {};
            text-align: centered;
            font-size: 2rem;
            color: white;
            user-select: none;
            padding: 4rem;
            margin: 0;
            ",
            match self.state {
                State::Init | State::Show => "blue",
                State::DenyClick => "red",
                State::AcceptClick => "green",
            }
        );
        let text = match self.state {
            State::Init => "Click on blue to begin reaction test".to_owned(),
            State::DenyClick => "Wait for green".to_owned(),
            State::AcceptClick => "!!!".to_owned(),
            State::Show => {
                if let Some(duration) = self.duration_millis() {
                    format!("{} ms", duration)
                } else {
                    "Oops, something went wrong".to_owned()
                }
            }
        };
        let onclick = self.link.callback(|_| Msg::Click);
        let mean: f64 = self.history.iter().copied().sum::<f64>() / (self.history.len() as f64);
        let sd: f64 = (self
            .history
            .iter()
            .map(|val: &f64| (*val - mean).powf(2.0))
            .sum::<f64>()
            / (self.history.len() as f64 - 1.0))
            .powf(0.5);
        html! {
            <>
                <h2>{"Reaction Time Test"}</h2>
                <div style=style onclick=onclick> {text} </div>
                <h2>{"History"}</h2>
                <pre>{format!("mean: {}\n", mean)}</pre>
                <pre>{format!("standard deviation: {}\n", sd)}</pre>
                <pre>{format!("{:?}", self.history)}</pre>
            </>
        }
    }
}

impl App {
    fn duration_millis(&self) -> Option<f64> {
        let begin = self.time_begin?;
        let end = self.time_end?;
        let micros: i64 = (end - begin).num_microseconds()?;
        Some(micros as f64 / 1000.0)
    }
}
