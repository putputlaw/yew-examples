import init, { run_app } from './pkg/reaction_times.js';
async function main() {
   await init('./pkg/reaction_times_bg.wasm');
   run_app();
}
main()
