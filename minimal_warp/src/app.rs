use crate::api::*;
use yew::*;
use yew::{
    format::Json,
    services::{
        fetch::{FetchTask, Request, Response},
        FetchService,
    },
};

pub type Result<T> = std::result::Result<T, anyhow::Error>;

pub struct App {
    link: ComponentLink<Self>,
    props: Props,
    fetch: FetchService,
    tasks: Vec<FetchTask>,
    received: Option<Download>,
    url: String,
}

#[derive(Clone, Properties, Debug, Default)]
pub struct Props {}

#[derive(Clone, Debug)]
pub enum Msg {
    Ignore,
    Request(DownloadTask),
    Receive(Download),
    UrlChanged(String),
}

impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            props,
            fetch: FetchService::default(),
            tasks: vec![],
            received: None,
            url: String::new(),
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => return false,
            Msg::Request(task) => {
                let request = Request::post("http://127.0.0.1:1111/api/download")
                    .header("Content-Type", "application/json")
                    .body(Json(&task)) // NOTE need the borrow, otherwise Into<Result<String>> trait undefined
                    .expect("failed to build request");
                let callback = self
                    .link
                    .callback(|response: Response<Json<Result<Download>>>| {
                        if let (meta, Json(Ok(body))) = response.into_parts() {
                            if meta.status.is_success() {
                                return Msg::Receive(body);
                            }
                        }
                        Msg::Ignore
                    });

                if let Ok(fetch_task) = self.fetch.fetch(request, callback) {
                    self.tasks.push(fetch_task);
                }
            }
            Msg::Receive(download) => self.received = Some(download),
            Msg::UrlChanged(url) => self.url = url,
        }
        true
    }

    fn view(&self) -> Html {
        let url = self.url.clone();
        let onclick = self.link.callback(move |_|Msg::Request(DownloadTask::new(&url)));
        let oninput = self.link.callback(|ev: InputData| Msg::UrlChanged(ev.value));

        html! {
            <>
                <h1>{"Download Service"}</h1>
                <div>
                    <input value=&self.url oninput=oninput />
                    <button onclick=onclick>{"Download"}</button>
                    <br />
                        {format!("Received Bytes: {}",
                            self.received.iter().map(|r| r.as_bytes().len()).next().unwrap_or(0))}
                </div>
            </>
        }
    }
}
