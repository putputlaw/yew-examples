#![recursion_limit = "1024"]

#[macro_use]
extern crate serde_derive;

use wasm_bindgen::prelude::*;
use yew::start_app;

pub mod app;
pub mod api;

#[wasm_bindgen]
pub fn run_app() {
    start_app::<app::App>();
}
