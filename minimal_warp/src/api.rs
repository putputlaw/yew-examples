#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DownloadTask {
    url: String,
}

impl DownloadTask {
    pub fn new(url: &str) -> Self {
        Self { url: url.to_owned() }
    }

    pub fn url(&self) -> &str {
        &self.url
    }
}


#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Download {
    data: Vec<u8>,
}

impl Download {
    pub fn new(data: &[u8]) -> Self {
        Self { data: data.to_vec() }
    }

    pub fn into_bytes(self) -> Vec<u8> {
        self.data
    }

    pub fn as_bytes(&self) -> &[u8] {
        &self.data
    }
}
