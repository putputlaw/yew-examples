use hyper::{Client, Uri};
use minimal::api::*;
use warp::Filter;
use hyper_tls::HttpsConnector;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Serve files
    let files = warp::fs::dir("./");

    // A simple service for downloading files
    let download = warp::path!("download")
        .and(warp::post())
        .and(warp::body::json())
        .and_then(|task: DownloadTask| async move {
            let https = HttpsConnector::new();
            let client = Client::builder().build::<_, hyper::Body>(https);
            if let Ok(uri) = task.url().parse::<Uri>() {
                if let Ok(response) = client.get(uri).await {
                    if let Ok(buffer) = hyper::body::to_bytes(response).await {
                        return Ok(warp::reply::json(&Download::new(&buffer)));
                    }
                }
            }
            Err(warp::reject::not_found())
        });

    let api = warp::path("api").and(download);
    let routes = api.or(files);

    println!("The server is running on http://127.0.0.1:1111");
    warp::serve(routes).run(([127, 0, 0, 1], 1111)).await;
    Ok(())
}
