#![recursion_limit = "1024"]

use wasm_bindgen::prelude::*;
use yew::start_app;

pub mod app;
pub mod opp;
pub mod shared;

#[wasm_bindgen]
pub fn run_app() {
    start_app::<app::App>();
}
