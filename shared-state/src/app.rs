use super::opp::{Opp, OppData};
use super::shared::*;
use yew::*;

pub struct App {
    link: ComponentLink<Self>,
    props: Props,
    opp1: Shared<OppData>,
    opp2: Shared<OppData>,
    task: yew::services::interval::IntervalTask,

}

#[derive(Clone, Properties, Debug, Default)]
pub struct Props {}

#[derive(Clone, Debug)]
pub enum Msg {
    Ignore,
    Refresh,
}

impl Component for App {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let opp1 = share(vec![-1, 2]);
        let opp2 = share(vec![-2, 4]);
        let task = yew::services::IntervalService::new().spawn(std::time::Duration::from_secs(2), link.callback(|_| Msg::Refresh));
        Self {
            link,
            props,
            opp1,
            opp2,
            task,
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => return false,
            Msg::Refresh => {
                // HACK
                self.opp1 = self.opp1.clone();
            }
        }
        true
    }

    fn view(&self) -> Html {
        html! {
            <div>
                <div>
                    <h1>{"Opp Monitor"}</h1>
                    <p>{"Refreshes every three seconds"}</p>
                    <code> {format!("opp1 = {:?}", self.opp1.borrow())} </code>
                    <br />
                    <code> {format!("opp2 = {:?}", self.opp2.borrow())} </code>
                </div>
                <hr />
                <Opp init=self.opp1.clone()/>
                <hr />
                <Opp init=self.opp2.clone()/>
            </div>
        }
    }
}
