use super::shared::*;
use yew::*;

pub type OppData = Vec<i64>;

pub struct Opp {
    link: ComponentLink<Self>,
    props: Props,
    data: Shared<OppData>,
}

#[derive(Clone, Properties, Debug, Default)]
pub struct Props {
    #[prop_or_default]
    pub init: Option<Shared<OppData>>,
}

#[derive(Clone, Debug)]
pub enum Msg {
    Ignore,
    AppendSum,
}

impl Component for Opp {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            data: props.init.clone().unwrap_or_else(|| share(vec![])),
            link,
            props,
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Ignore => return false,
            Msg::AppendSum => {
                let sum = self.data.borrow().iter().sum::<i64>();
                self.data.borrow_mut().push(sum);
            }
        }
        true
    }

    fn view(&self) -> Html {
        let onclick = self.link.callback(|_| Msg::AppendSum);

        html! {
            <div>
                <h2>{"Opp instance"}</h2>
                <button onclick=onclick>{"append sum"}</button> <br />
                <code>{format!("data = {:?}", self.data.borrow())}</code>
            </div>
        }
    }
}
