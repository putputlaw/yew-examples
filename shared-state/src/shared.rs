use std::rc::Rc;
use std::cell::RefCell;

pub type Shared<T> = Rc<RefCell<T>>;

pub fn share<T>(data: T) -> Shared<T> {
    Rc::new(RefCell::new(data))
}
